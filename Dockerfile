FROM frolvlad/alpine-python-machinelearning

COPY . .
RUN apk add --no-cache gcc g++ libc-dev unixodbc-dev python3-dev
RUN pip install -r requirements.txt
