#!/usr/bin/env bash

surprise_version=1.0.6
lightfm_versions=1.15
tag=python-surprise-${surprise_version}-lightfm-${lightfm_versions}
image=tjugo/python-surprise-lighfm:${tag}

docker build \
      -t ${image} \
      .
docker push ${image}
echo "Built ${image}"

